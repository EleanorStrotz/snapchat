package android.strotze.snapchat;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainMenuFragment extends Fragment {

    private static final int REQUEST_CHOOSE_PHOTO = 2;

    public MainMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);

        String[] menuItems = {"Login",
                                 "Register"};

        final ListView listView = (ListView) view.findViewById(R.id.mainMenu);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                menuItems
        );

        listView.setAdapter(listViewAdapter);

        //listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          //  @Override
            //public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              //  if (position == 0){
                //    Intent intent = new Intent(getActivity(), LoginActivity.class);
                  //  startActivity(intent);
                //}else if (position == 1){
                  //  Intent intent = new Intent(getActivity(), RegisterActivity.class);
                    //startActivity(intent);
                //}
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                        if (position == 0){
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                                   REQUEST_CHOOSE_PHOTO);
                        }
                    }
                });
           // }
       // });

        // Inflate the layout for this fragment
        return view;
    }
@Override
    public void onActivityResult(int requestCode,int resultCode, Intent data){
    if(requestCode == REQUEST_CHOOSE_PHOTO){
        if(resultCode == Activity.RESULT_OK){
            Uri uri = data.getData();
            Intent intent = new Intent(getActivity(), FriendListActivity.class);
            intent.putExtra("ImageURI", uri);
            startActivity(intent);
        }
    }
}
}
