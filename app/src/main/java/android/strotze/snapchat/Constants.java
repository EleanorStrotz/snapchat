package android.strotze.snapchat;

/**
 * Created by Student on 5/22/2016.
 */
public class Constants {
    public static final String ACTION_ADD_FRIEND = "android.strotze.snapchat.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "android.strotze.snapchat.SEND_FRIEND_REQUEST";
    public static final String ACTION_SEND_PHOTO = "android.strotze.snapchat.SEND_PHOTO";

    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "android.strotze.snapchat.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "android.strotze.snapchat.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "android.strotze.snapchat.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "android.strotze.snapchat.FRIEND_REQUEST_FAILURE";
}
