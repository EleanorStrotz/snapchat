package android.strotze.snapchat;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "D1C65C9E-61D3-B6CE-FFDE-7199F3089700";
    public static final String SECRET_KEY="527B98E4-D428-412D-FF70-3BA2FACF4D00";
    public static final String VERSION="v1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser() == ""){
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        }else{
            LoggedInMenuFragment loggedIn = new LoggedInMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loggedIn).commit();
        }

    }
}
