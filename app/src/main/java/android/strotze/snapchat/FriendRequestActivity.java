package android.strotze.snapchat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Student on 5/24/2016.
 */
public class FriendRequestActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FriendRequestFragment FriendRequest = new FriendRequestFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, FriendRequest).commit();
}}
