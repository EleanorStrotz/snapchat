package android.strotze.snapchat;

/**
 * Created by Student on 5/25/2016.
 */
public class SentPicture {

    private boolean viewed;
    private String fromUser;
    private String toUser;
    private String imageLocation;

    public SentPicture(){
        viewed = false;
        fromUser = "";
        toUser = "";
        imageLocation = "";
    }
    public boolean isViewed(){
        return viewed;
    }
    public String getToUser(){
        return toUser;
    }
    public void setToUser(String user){
        toUser = user;
    }
    public String getFromUser(){
        return fromUser;
    }
    public void setFromUser(String user){
        fromUser = user;
    }
    public String getImageLocation(){
        return imageLocation;
    }
    public void setImageLocation(String location){
        imageLocation = location;
    }
}
