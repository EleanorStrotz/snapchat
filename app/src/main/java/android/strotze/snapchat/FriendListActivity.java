package android.strotze.snapchat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Student on 5/22/2016.
 */
public class FriendListActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FriendListFragment FriendList = new FriendListFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, FriendList).commit(); }
}
